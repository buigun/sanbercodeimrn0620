///soal no 1///
let flag1 = 2;
let flag2 = 20;

console.log('LOOPING PERTAMA');

while (flag1 <= 20) {
    console.log(`${flag1} - I love coding`);
    flag1 += 2;
}

console.log('LOOPING KEDUA');

while (flag2 > 0) {
    console.log(`${flag2} - I will become a mobile developer`);
    flag2 -= 2;
}

console.log('');
///soal no 2///
for (let i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
        console.log(`${i} - I Love Coding`);
    } else if (i % 2 == 0) {
        console.log(`${i} - Berkualitas`);
    } else {
        console.log(`${i} - Santai`);
    }
}

console.log('');
///soal no 3///
let result3 = '';
for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 8; j++) {
        result3 += '#';
    }
    result3 += '\n';
}

console.log(result3);

console.log('');
///soal no 4///
let result4 = '';
for (let i = 0; i < 7; i++) {
    for (let j = 0; j <= i; j++) {
        result4 += '#';
    }
    result4 += '\n';
}

console.log(result4);

console.log('');
///soal no 5///
let result5 = '';
for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
        if (i % 2 == 0) {
            if (j % 2 == 0) {
                result5 += ' ';
            } else {
                result5 += '#';
            }
        } else {
            if (j % 2 == 0) {
                result5 += '#';
            } else {
                result5 += ' ';
            }
        }
    }
    result5 += '\n';
}

console.log(result5);