var nama = 'Budi';
var peran = 'Werewolf';

if (!nama) {
    console.log('Nama harus diisi!')
} else if (nama && !peran) {
    console.log(`Halo ${nama}, Pilihlah peranmu untuk memulai game!`)
} else {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    if (peran == 'Penyihir') {
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
    } else if (peran == 'Guard') {
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
    } else if (peran == 'Werewolf') {
        console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
    } else {
        console.log('Peran yang tersedia: "Penyihir", "Guard", "Werewolf"')
    }
}

///soal no 2//
console.log(" ")
var tanggal = 17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var bulanString = ''

switch (bulan) {
    case 1:
        bulanString = 'Januari'
        break;
    case 2:
        bulanString = 'Februari'
        break;
    case 3:
        bulanString = 'Maret'
        break;
    case 4:
        bulanString = 'April'
        break;
    case 5:
        bulanString = 'Mei'
        break;
    case 6:
        bulanString = 'Juni'
        break;
    case 7:
        bulanString = 'Juli'
        break;
    case 8:
        bulanString = 'Agustus'
        break;
    case 9:
        bulanString = 'September'
        break;
    case 10:
        bulanString = 'Oktober'
        break;
    case 11:
        bulanString = 'November'
        break;
    case 12:
        bulanString = 'Desember'
        break;
    default:
        console.log('Input bulan salah')
        break;
}

console.log(`${tanggal} ${bulanString} ${tahun}`)