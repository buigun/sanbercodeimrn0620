///soal no 1///
console.log("Soal no 1 :");
function arrayToObject(arr) {
  if (arr.length == 0) {
    console.log('""');
  }

  const now = new Date();
  const year = now.getFullYear();

  for (let i = 0; i < arr.length; i++) {
    let umur;

    if (arr[i][3] === undefined || arr[i][3] > year) {
      umur = "Invalid Birth Year";
    } else {
      umur = year - arr[i][3];
    }

    let ObjTemp = {
      firstName: arr[i][0],
      lastName: arr[i][1],
      gender: arr[i][2],
      age: umur,
    };

    console.log(`${i + 1}. ${arr[i][0]} ${arr[i][1]}:`);
    console.log(ObjTemp);
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

console.log("");
console.log("Soal no 2 :");
///soal no 2///

function shoppingTime(memberId, money) {
  const sale = [
    {
      product: "Sepatu Stacattu",
      price: 1500000,
    },
    {
      product: "Baju Zoro",
      price: 500000,
    },
    {
      product: "Baju H&N",
      price: 250000,
    },
    {
      product: "Sweater Uniklooh",
      price: 175000,
    },
    {
      product: "Casing handphone",
      price: 50000,
    },
  ];

  let result = {
    memberId,
    money,
    listPurchased: [],
    changeMoney: money,
  };

  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (memberId && money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }

  for (let i = 0; i < sale.length; i++) {
    if (money >= sale[i].price) {
      money -= sale[i].price;
      result.listPurchased.push(sale[i].product);
      result.changeMoney = money;
    }
  }

  return result;
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(" ");
console.log("Soal no 3 :");
///soal no 3///

function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  let result = [];

  if (arrPenumpang.length == 0) {
    return result;
  }

  for (let i = 0; i < arrPenumpang.length; i++) {
    let bayar =
      (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) *
      2000;

    let objTemp = {
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2],
      bayar,
    };

    result.push(objTemp);
  }

  return result;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
