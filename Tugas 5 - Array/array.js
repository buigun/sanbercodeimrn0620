console.log("Soal no 1 :");

function range(num1, num2) {
  if (!num1 || !num2) {
    return -1;
  }

  let result = [];

  if (num1 < num2) {
    for (let i = num1; i <= num2; i++) {
      result.push(i);
    }
  } else {
    for (let i = num1; i >= num2; i--) {
      result.push(i);
    }
  }

  return result;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1
console.log("");

console.log("Soal no 2 :")
function rangeWithStep(num1, num2, range) {
  if (!num1 || !num2) {
    return -1;
  }

  let result = [];

  if (num1 < num2) {
    for (let i = num1; i <= num2; i += range) {
      result.push(i);
    }
  } else {
    for (let i = num1; i >= num2; i -= range) {
      result.push(i);
    }
  }

  return result;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
console.log("");

console.log("Soal no 3 :")
function sum(num1, num2, range) {
  let result = 0;
  let array = [];

  if (!num1) {
    return result;
  } else if (num1 && !num2) {
    result = num1;
    return result;
  }

  if (!range) {
    array = rangeWithStep(num1, num2, 1);
    array.forEach((num) => {
      result += num;
    });
    return result;
  } else {
    array = rangeWithStep(num1, num2, range);
    array.forEach((num) => {
      result += num;
    });
    return result;
  }
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("");

console.log("Soal no 4 :")

function dataHandling(data) {
    data.forEach((user)=>{
        let output = `
        Nomor ID: ${user[0]}
        Nama Lengkap: ${user[1]}
        TTL: ${user[2]} ${user[3]}
        Hobi: ${user[4]}
        `

        console.log(output);
    })
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)
console.log("");

console.log("Soal no 5 :");

function balikKata(input) {
    let result = '';

    for (let i = input.length-1; i >= 0; i--) {
        result += input[i];
    }

    return result;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("");

console.log("Soal no 6 :");

function dataHandling2(input) {
    input.splice(1,2, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung');
    input.splice(4,1, 'Pria', 'SMA Internasional Metro');
    console.log(input);

    var tanggalLahir = input[3].split("/");
    switch(tanggalLahir[1]) {
        case '01' :
            console.log('Januari');
            break;
        case '02' :
            console.log('Februari');
            break;
        case '03' :
            console.log('Maret');
            break;
        case '04' :
            console.log('April');
            break;
        case '05' :
            console.log('Mei');
            break;
        case '06' :
            console.log('Juni');
            break;
        case '07' :
            console.log('Juli');
            break;
        case '08' :
            console.log('Agustus');
            break;
        case '09' :
            console.log('September');
            break;
        case '10' :
            console.log('Oktober');
            break;
        case '11' :
            console.log('November');
            break;
        case '12' :
            console.log('Desember');
            break;
    }

    var tanggalLahirStrip = tanggalLahir.join('-')

    tanggalLahir.sort(function(a,b){return b-a});
    console.log(tanggalLahir);

    console.log(tanggalLahirStrip);

    console.log(input[1].slice(0,15));
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
